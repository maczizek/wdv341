<?php session_start();
if($_SESSION['validUser'] == "admin")
	{/*display admin view*/}
	else if($_SESSION['validUser'] == "member")
	{/*display regular view*/}
	else
	{header('Location: index.php');}
?>
<!DOCTYPE html>
<html>
  <head>

    <title>By Laws</title>
	<meta name="keywords" content="Renaissance Homeowners Association Ankeny Iowa" />
	<meta name="description" content="Renaissance Homeowners Association is a homeowners association located in Ankeny, Iowa." />
	<link href="files/generalStyles.css" rel="stylesheet" type="text/css" />
	<style type="text/css">


	</style>
	<script>


	
	
	</script>
  </head>

<body>
<section id="container">
	<header>
	<div class="header"><img src="files/hoa_logo.jpg" alt="RHA logo" title="RHA logo" width="200px" id="logo">
	<h1 class="head">Renaissance Homeowners Association</h1>
	</div>
	</header>
	<nav>
	
	</nav>
	<aside><?php if($_SESSION['validUser'] == "member"){echo '<strong>Hello, Association Member</strong></br>
	<ul>
	<li><a href="index.php">Home Page</a></li>
	<li><a href="membersList.php">Listing of all members</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>';}
	else if($_SESSION['validUser'] == "admin"){
	echo '<strong>Hello, Admin</strong></br>
	<ul>
	<li><a href="index.php">Home Page</a></li>
	<li><a href="membersList.php">Listing of all members</a></li>
	<li><a href="insertUser.php">Add New Member</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>';		
	}
	else
	{echo "Member's Login</br>
	<form method='post' action='login.php'>
<input type='text' name='username' id='username' placeholder='Username' value='maczizek' />
<input type='password' name='password' id='password' placeholder='Password' value='wdv341' />
<input type='submit' name='submitLogin' Value='Sign In' /></br>";} if($_SESSION['signInErrors'] == true){echo "Either your username or password is not correct.  Please try to reenter or contact RHA.</form>"
	;} ?>
	
	</aside>
	<section id="main">
	
	<h2>By Laws & Covenants</h2>
<h3>Section I. Personal Application.</h3>

<p>All present or future Owners, tenants, future tenants, or their employees, or any other person who might use the facilities of the Project in any manner, are subject to the regulations set forth in these bylaws and all governing documents of the Association. The mere acquisition or rental of any of the Lots of the Project or the mere act of occupancy of any of the Lots will signify that these bylaws are accepted, ratified and will be complied with.</p>

<h2>ARTICLE 11</h2>

<h3>MEMBERSHIP VOTING AND MEETING OF MEMBERS</h3>

<p><strong>A. MEMBERSHIP AND VOTING</strong></p>

<p><strong>Section 1.</strong> Membership, Every person or entity who is a record Owner of a fee or undivided fee interest in any Lot which is subject by covenants of record to assessment by the Association, including contract sellers, shall be a member of the Association.<p>

<p>The foregoing is not intended to include persons or entities who hold an interest merely as security for the performance of an obligation. Membership shall be appurtenant to and may not be separated from ownership of any Lot which is subject to assessment by the Association. Ownership of such Lot shall be the sole qualification for membership, and such membership shall continue until such time as the Owner's ownership terminates, at which time his/her membership shall automatically cease.<p>

<p>Proof of membership (such as a grant deed), if called for by the. Association or its managing agent, must be provided to the Secretary of the Association (or other designated representative) prior to any rights of membership being exercised.</p>

<p><strong>Section 2.</strong> Voting of Member. Members shall be all those Owners as defined above. Voting rights are based on one vote per Lot owned. When more Om one person holds title, all such persons collectively shall be the member (for the Lot in question). The vote shall be exercised as they among themselves determine, but in no event shall more than one vote be cast with respect to any Lot. The Association shall be entitled to presume that any ballot tendered by one or more Owners of the Lot was the result of agreement by all other Owners. If conflicting ballots are cast by Owners, none will be counted.<p>

<p><strong>Section 3.</strong> Suspension of Member's Rights, Members are subject to suspension of membership for voting purposes and for purposes of use of the recreational facilities when their assessment payments fall delinquent or a violation of these Amended bylaws, the Restated Declaration, or the rules and regulations occurs. Refer to the Restated Declaration for the limitations and notice provisions relating to suspensions of membership.<p>
	
	
	
	</section><!-- end main -->
	<footer>
	<h6>Copyright &copy; 2014, by MaryAnn Czizek. All rights reserved. All text, graphics, and HTML code are protected by US and International Copyright Laws, and may not be copied, reprinted, published, translated, hosted, or otherwise distributed by any means without explicit permission.</h6>
	
	</footer>
	
</section> <!-- end container -->
</body>
</html>