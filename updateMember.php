<?php session_start();

if($_SESSION['validUser'] != "admin")
	{header('Location: index.php');}

$updateRecId = $_GET["recId"];
include 'dbConnect.php';
include "files/validationTests.php";
$sql = "SELECT * FROM hoa_members WHERE member_id=$updateRecId";	
$result = mysqli_query($link,$sql);
$row = mysqli_fetch_array($result);
				
if(isset($_POST["confirmation"])) //came from the form
{
			if($_POST["confirmUpdate"] == 'Yes') //from form and wants edit
			{


				
				
				$fName = test_input($_POST["fName"]);
				$lName = test_input($_POST["lName"]);
				$unit = test_input($_POST["unit"]);
				$email = test_input($_POST['email']);
				$phone = test_input($_POST['phone']);
				$username = test_input($_POST["username"]);
				$password = test_input($_POST['password']);
	
				$validForm = true;
				validateName();
				validateUnit();	
				validateEmail();
				validatePhone();
				validateUsername();
				validatePassword();
		
				if($validForm == true)
				{
					$sql = "UPDATE hoa_members SET " ;
					$sql .= "fName='$fName', ";
					$sql .= "lName='$lName', ";
					$sql .= "unit='$unit', ";
					$sql .= "email='$email', ";		
					$sql .= "phone='$phone', ";
					$sql .= "username='$username', ";
					$sql .= "password='$password' ";
					$sql .= " WHERE (member_id='$updateRecId')";
					if (mysqli_query($link,$sql) )
				{}
				}
			}
			else  //came from the form, but denied the edit
			{header('Location: index.php');}
	
}

?>
<!DOCTYPE html>
<html>
  <head>

    <title>RHA | List of Members</title>
	<meta name="keywords" content="Renaissance Homeowners Association Ankeny Iowa" />
	<meta name="description" content="Renaissance Homeowners Association is a homeowners association located in Ankeny, Iowa." />
	<link href="files/generalStyles.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.error{
			color: red;
		}


	</style>
	<script>


	
	
	</script>
  </head>

<body>
<section id="container">
	<header>
	<div class="header"><img src="files/hoa_logo.jpg" alt="RHA logo" title="RHA logo" width="200px" id="logo">
	<h1 class="head">Renaissance Homeowners Association</h1>
	</div>
	</header>
	<nav>
	
	</nav>
	<aside><?php if($_SESSION['validUser'] == "member"){echo '<strong>Hello, Association Member</strong></br>
	<ul>
	<li><a href="membersList.php">Listing of all members</a></li>
	<li><a href="byLaws.php">By Laws & Covenants</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>';}
	else if($_SESSION['validUser'] == "admin"){
	echo '<strong>Hello, Admin</strong></br>
	<ul>
	<li><a href="membersList.php">Listing of all members</a></li>
	<li><a href="byLaws.php">By Laws & Covenants</a></li>
	<li><a href="insertUser.php">Add New Member</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>';		
	}
	else
	{echo "Member's Login</br>
	<form method='post' action='login.php'>
<input type='text' name='username' id='username' placeholder='Username' value='maczizek' />
<input type='password' name='password' id='password' placeholder='Password' value='wdv341' />
<input type='submit' name='submitLogin' Value='Sign In' /></form>";} ?>
	</aside>
	<section id="main">
	
	<?php 
	if( isset($_POST['confirmation']) ){
		//if (mysqli_query($link,$sql) )
		if($validForm == true)
		{
			echo "<h1>Thank you for your edit.  The record has been successfully updated in the database.</h1>";
			echo "<p>Please <a href='membersList.php'>view</a> your records.</p>";
		}
		/*else
		{
			echo "<h1>There has been a problem.</h1>";
			echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}*/
	}
	if ($validForm == false || isset($_POST['confirmation']) == false){
	?>
	
	<p><form action="updateMember.php?recId=<?php echo $_GET['recId']; ?>" method="post" >
<fieldset>
    <legend>Update Member's Information</legend>
	<table>
	
<tr><td>Member's First Name</td> <td><input type="text" name="fName" id="fName" value="<?php echo $fName; ?>"  /></td><td width="210" class="error"><?php echo "$fNameErrMsg";  ?></td></tr>

<tr><td>Member's Last Name</td> <td><input type="text" name="lName" id="lName" value="<?php echo $lName; ?>"  /></td><td width="210" class="error"><?php echo "$lNameErrMsg";  ?></td></tr>

<tr><td>Unit</td> <td><input type="text" name="unit" id="unit" value="<?php echo $unit; ?>"  /></td><td width="210" class="error"><?php echo "$unitErrMsg";  ?></td></tr>

<tr><td>Email</td> <td><input type="text" name="email" id="email" value="<?php echo $email; ?>"  /></td><td width="210" class="error"><?php echo "$emailErrMsg";  ?></td></tr>

<tr><td>Phone</td> <td><input type="text" name="phone" id="phone" value="<?php echo $phone; ?>"  /></td><td width="210" class="error"><?php echo "$phoneErrMsg";  ?></td></tr>

<tr><td>Username</td> <td><input type="text" name="username" id="username" value="<?php echo $username; ?>"  /></td><td width="210" class="error"><?php echo "$usernameErrMsg";  ?></td></tr>

<tr><td>Password</td> <td><input type="text" name="password" id="password" value="<?php echo $password; ?>"  /></td><td width="210" class="error"><?php echo "$passwordErrMsg";  ?></td></tr>

<!--<tr><td>Admin Rights</td> <td>Yes: 
<input type="radio" name="adminRights" id="adminRights" value="<?php  if($row['adminRights']=="1"){echo "checked='checked'";} ?>"  />
No: <input type="radio" name="adminRights" id="adminRights" value="<?php if($row['adminRights']=="0"){echo "checked='checked'";} ?>"  /></td></tr>
-->
</table>

<p><strong>Does this information look correct, and you would like to enact the edit?</strong></p>
<input type="radio" name="confirmUpdate" id="confirmUpdate" value="Yes" >Yes
<input type="radio" name="confirmUpdate" id="confirmUpdate" value="No" >No

<input type="submit" name="confirmation" id="confirmation" value="Submit">

</fieldset>
</form>

	<?php } // end inside section

?>
	
	
	
	</section><!-- end main -->
	<footer>
	<h6>Copyright &copy; 2014, by MaryAnn Czizek. All rights reserved. All text, graphics, and HTML code are protected by US and International Copyright Laws, and may not be copied, reprinted, published, translated, hosted, or otherwise distributed by any means without explicit permission.</h6>
	
	</footer>
	
</section> <!-- end container -->
</body>
</html>
<?php 
 	mysqli_close($link);
?>