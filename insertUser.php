<?php session_start(); 
	if($_SESSION['validUser'] != "admin")
	{header('Location: index.php');}
	
	if( isset($_POST['submit']) ){
	
		include "dbConnect.php";
		include "files/validationTests.php";
		
		foreach($_POST as $key => $value)
		{
		$body.= $key."=".$value."\n";
		} 
		
		date_default_timezone_set('America/Indiana/Knox');
		$date = new DateTime();
		$date = $date->format('Y-m-d');
		
		$time = new DateTime();
		$time = $time->format('H:i:s');
		
		//form validation

		
		//$member_id = test_input($_POST["member_id"]);
		$fName = test_input($_POST["fName"]);
		$lName = test_input($_POST["lName"]);	
		$unit = test_input($_POST["unit"]);
		$email = test_input($_POST["email"]);
		$phone = test_input($_POST["phone"]);
		$listPhone = test_input($_POST["listPhone"]);
		$username = test_input($_POST["username"]);
		$password = test_input($_POST["password"]);
		$adminRights = test_input($_POST["adminRights"]);
		
		$validForm = true;
		validateName();
		validateUnit();	
		validateEmail();
		validatePhone();
		validateUsername();
		validatePassword();
		
		if($validForm == true)
		{$sql = "INSERT INTO hoa_members (member_id,fName,lName,unit,email,phone,listPhone,username,password, adminRights)VALUES ('','$fName','$lName','$unit','$email','$phone','$listPhone','$username','$password','$adminRights')";
		}
	}
	?>
    

<!DOCTYPE html>
<html>
  <head>

    <title>Renaissance Homeowners Association</title>
	<meta name="keywords" content="Renaissance Homeowners Association Ankeny Iowa" />
	<meta name="description" content="Renaissance Homeowners Association is a homeowners association located in Ankeny, Iowa." />
	<link href="files/generalStyles.css" rel="stylesheet" type="text/css" />
	<link href="validationTests.php" />
	<style type="text/css">
		.error{
			color: red;
		}

	</style>
	<script>


	
	
	</script>
  </head>

<body>
<section id="container">
	<header>
	<div class="header"><img src="files/hoa_logo.jpg" alt="RHA logo" title="RHA logo" width="200px" id="logo">
	<h1 class="head">Renaissance Homeowners Association</h1>
	</div>
	</header>
	<nav>
	
	</nav>
	<aside><?php if($_SESSION['validUser'] == "admin"){echo '<strong>Hello, Admin</strong></br>
	<ul>
	<li><a href="index.php">Home Page</a></li>
	<li><a href="membersList.php">Listing of all members</a></li>
	<li><a href="byLaws.php">By Laws & Covenants</a></li>
	<li><a href="insertUser.php">Add New Member</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>';}
	 ?>
	
	</aside>
	<section id="main">
<?php 
	if( isset($_POST['submit']) ){
		//if (mysqli_query($link,$sql) )
		if($validForm == true)
		{
			echo "<h2>Your new member has been successfully added to the database.</h2>";
			echo "<p>Please <a href='membersList.php'>view</a> your member list or <a href='insertUser.php'>add</a> another member.</p>";
		}
		else
		{
			//echo "<h1>You have encountered a problem.</h1>";
			//echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}
	}
	if($validForm == false || isset($_POST['submit']) == false){	
	?>
	<h3>Admins, please complete the following to create a new member file.</h3>


<form id="form1" name="form1" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

  <fieldset>
    <legend>New Member Registration</legend>
  <table border="0">
    <tr>
      <td>First Name</td>
      <td><input type="text" name="fName" id="fName" value="<?php echo $fName; ?>"/></td><td width="210" class="error"><?php echo "$fNameErrMsg";  ?></td>
    </tr>
	
    <tr>
      <td>Last Name</td>
      <td><input type="text" name="lName" id="lName" value="<?php echo $lName; ?>"/></td><td width="210" class="error"><?php echo "$lNameErrMsg";  ?></td>
    </tr>
	
    <tr>
      <td>Unit Number</td>
      <td><input type="text" name="unit" id="unit" maxlength="3" value="<?php echo $unit; ?>"/></td><td width="210" class="error"><?php echo "$unitErrMsg";  ?></td>
    </tr>
	
     <tr>
      <td>Email</td>
      <td><input type="text" name="email" id="email" value="<?php echo $email; ?>"/></td><td width="210" class="error"><?php echo "$emailErrMsg";  ?></td>
    </tr>
	
     <tr>
      <td>Phone Number</td>
      <td><input type="text" name="phone" id="phone" value="<?php echo $phone; ?>"/></td><td width="210" class="error"><?php echo "$phoneErrMsg";  ?></td>
    </tr>
	
     <tr>
      <td>List His/Her Phone in Member's Listing?</td>
      <td>Yes: <input type="radio" name="phoneListing" value="1" checked="checked" /> No: <input type="radio" name="phoneListing" value="0" /></td>
    </tr>
	
     <tr>
      <td>Username for Login System</td>
      <td><input type="text" name="username" id="username" value="<?php echo $username; ?>"/></td><td width="210" class="error"><?php echo "$usernameErrMsg";  ?></td>
    </tr>
	
     <tr>
      <td>Password for Login System</td><!--I chose not to make this password field encoded on the screen so that the admin knows he/she entered it correctly.-->
      <td><input type="text" name="password" id="password" value="<?php echo $password; ?>"/></td><td width="210" class="error"><?php echo "$passwordErrMsg";  ?></td>
    </tr>
	
	<tr>
      <td>Admin Rights?</td>
      <td>No: <input type="radio" name="adminRights" value="0" checked="checked" /> Yes: <input type="radio" name="adminRights" value="1" /></td>
    </tr>

  </table>
  <p>
    <input type="submit" name="submit" id="submit" value="Register" />
    <input type="reset" name="reset" id="reset" value="Try again!" />
  </p>


  </fieldset>
</form> 

	<?php }
	//else {} // end inside section

?>
	
	
	</section><!-- end main -->
	<footer>
	<h6>Copyright &copy; 2014, by MaryAnn Czizek. All rights reserved. All text, graphics, and HTML code are protected by US and International Copyright Laws, and may not be copied, reprinted, published, translated, hosted, or otherwise distributed by any means without explicit permission.</h6>
	
	</footer>
	
</section> <!-- end container -->
</body>
</html>
