<?php
function test_input($data) {
			$data = trim($data);			//remove white spaces
			$data = stripslashes($data);	// removes back slashes
			$data = htmlspecialchars($data);//removes < >
			return $data;
		}	
		
		function validateName()
		{
			global $fName, $lName, $validForm, $fNameErrMsg, $lNameErrMsg;
			$fNameErrMsg = "";
			$lNameErrMsg = "";
			if($fName=="")
			{
				$validForm = false;	
				$fNameErrMsg = "Name is required";
			}
			if($lName=="")
			{
				$validForm = false;	
				$lNameErrMsg = "Name is required";
			}
		}
		
		function validateUnit()
		{
			global $unit, $validForm, $unitErrMsg;
			$unitErrMsg = "";

			if($unit == "")
			{
				$validForm = false;
				$unitErrMsg = "Unit is required.  ";
			}

			if( intval($unit)==0 )
			{
				$validForm = false;
				$unitErrMsg .= "Unit must be an integer.  ";
			}
			
			if( $unit > 400)		//REASONABLE VALIDATION TEST
			{
				$validForm = false;
				$unitErrMsg .= "RHA does not have units above 400.";		
			}
			else if($unit <1)		//REASONABLE VALIDATION TEST
			{
				$validForm = false;
				$unitErrMsg .= "Please enter a unit.";		
			}
			//else()

			if(strpos($unit, ".")!== false)		//check for period
			{
				$validForm = false;
				$unitErrMsg .= "Unit must be entered as a whole number.";		
			}
		}
		
		function validateEmail()
			{
				global $email, $validForm, $emailErrMsg;	
				$emailErrMsg = "";
				
				if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
				{
					$validForm = false;
					$emailErrMsg = "There has been an invalid email entered."; 
				}		
		}
		
		function validatePhone()
		{
			global $phone, $validForm, $phoneErrMsg;

			$phoneErrMsg = "";
			$expr = '/^\+?(\(?[0-9]{3}\)?|[0-9]{3})[-\.\s]?[0-9]{3}[-\.\s]?[0-9]{4}$/';
			if (preg_match($expr, $phone) != 1)
			{
				$validForm = false;
				$phoneErrMsg = "There has been an invalid phone number entered.";
			}
		}
		
		function validateUsername()
		{
			global $username, $validForm, $usernameErrMsg;
			$usernameErrMsg = "";

			if($username=="")
			{
				$validForm = false;	
				$usernameErrMsg = "Username is required";
			}
		}
		
		function validatePassword()
		{
			global $password, $validForm, $passwordErrMsg;
			$passwordErrMsg = "";

			if($password=="")
			{
				$validForm = false;	
				$passwordErrMsg = "Password is required";
			}
		}
?>