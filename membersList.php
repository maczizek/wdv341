<?php session_start(); 
if($_SESSION['validUser'] == "admin")
	{/*display admin view*/}
	else if($_SESSION['validUser'] == "member")
	{/*display regular view*/}
	else
	{header('Location: index.php');}
	
	include 'dbConnect.php';
	
	$sql = "SELECT * FROM hoa_members";
	$result = mysqli_query($link,$sql);
	if(!$result )							//Make sure the Query ran correctly and created result
	{
		echo "<h1 style='color:red'>We're sorry, but we have encountered an error in showing the listing of all association members.</h1>";	//Problems were encountered.
		echo mysqli_error($link);		//Display error message
	}
?>
<!DOCTYPE html>
<html>
  <head>

    <title>RHA | List of Members</title>
	<meta name="keywords" content="Renaissance Homeowners Association Ankeny Iowa" />
	<meta name="description" content="Renaissance Homeowners Association is a homeowners association located in Ankeny, Iowa." />
	<link href="files/generalStyles.css" rel="stylesheet" type="text/css" />
	<style type="text/css">


	</style>
	<script>


	
	
	</script>
  </head>

<body>
<section id="container">
	<header>
	<div class="header"><img src="files/hoa_logo.jpg" alt="RHA logo" title="RHA logo" width="200px" id="logo">
	<h1 class="head">Renaissance Homeowners Association</h1>
	</div>
	</header>
	<nav>
	
	</nav>
	<aside><?php if($_SESSION['validUser'] == "member"){echo '<strong>Hello, Association Member</strong></br>
	<ul>
	<li><a href="index.php">Home Page</a></li>
	<li><a href="byLaws.php">By Laws & Covenants</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>';}
	else if($_SESSION['validUser'] == "admin"){
	echo '<strong>Hello, Admin</strong></br>
	<ul>
	<li><a href="index.php">Home Page</a></li>
	<li><a href="byLaws.php">By Laws & Covenants</a></li>
	<li><a href="insertUser.php">Add New Member</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>';		
	}
	else
	{echo "Member's Login</br>
	<form method='post' action='login.php'>
<input type='text' name='username' id='username' placeholder='Username' value='maczizek' />
<input type='password' name='password' id='password' placeholder='Password' value='wdv341' />
<input type='submit' name='submitLogin' Value='Sign In' /></form>";} ?>
	</aside>
	<section id="main">
	
	<?php
	echo "<h3>" . mysqli_num_rows($result). " Members</h3>";	//display number of rows found by query
?>
<div>
	<table border="1">
	<tr>
		<th>Name</th>
		<th>Unit Number</th>
		<th>Email</th>
		<th>Phone</th>
		<?php if($_SESSION['validUser'] == "admin"){echo "<th>Update</th>";
		echo "<th>Delete</th>";} ?>
	</tr>    
<?php

	
	while($row = mysqli_fetch_array($result))		//Turn each row of the result into an associative array 
  	{
		//For each row you found in the table, create an HTML table in the response object
  		echo "<tr>";
  		echo "<td>" . $row['fName'] . " " . $row['lName'] . "</td>";
  		echo "<td>" . $row['unit'] . "</td>";
  		echo "<td>" . $row['email'] . "</td>";
  		echo "<td>" . $row['phone'] . "</td>";
		if($_SESSION['validUser'] == "admin"){echo "<td><a href='updateMember.php?recId=" . $row['member_id'] . "'>Update</a></td>";
		echo "<td><a href='deleteMember.php?recId=" . $row['member_id'] ."'>Delete</a></td>";}
  		echo "</tr>";
  	}

	//echo "</table>";		//Placed this command in the HTML instead of using the echo

	mysqli_close($link);		//Close the database connection to free up server resources.
?>
	</table>
</div>	


	
	
	
	</section><!-- end main -->
	<footer>
	<h6>Copyright &copy; 2014, by MaryAnn Czizek. All rights reserved. All text, graphics, and HTML code are protected by US and International Copyright Laws, and may not be copied, reprinted, published, translated, hosted, or otherwise distributed by any means without explicit permission.</h6>
	
	</footer>
	
</section> <!-- end container -->
</body>
</html>